package control;

import java.util.ArrayList;

import model.Animal;
import exception.InvalidAnimalTypeException;

public class AnimalControl {

	private ArrayList<Animal> animalList;

	public AnimalControl() {
		this.animalList = new ArrayList<Animal>();
	}

	public void add(Animal animal) {
		this.animalList.add(animal);
	}

	public boolean isEmpty() {
		return this.animalList.isEmpty();
	}

	public int count() {
		return this.animalList.size();
	}

	public Animal get(int index) throws IndexOutOfBoundsException {
		return this.animalList.get(index);
	}

	public int countType(Class<?> classType) {
		int count = 0;

		for (Animal animal : this.animalList) {
			if (classType.isInstance(animal)) {
				count++;
			}
		}

		return count;
	}

	public ArrayList<Animal> getAnimalsByType(Class<?> classType)
			throws InvalidAnimalTypeException {
		validateAnimalType(classType);

		ArrayList<Animal> animalsTypeList = new ArrayList<Animal>();

		for (Animal animal : this.animalList) {
			if (classType.isInstance(animal)) {
				animalsTypeList.add(animal);
			}
		}

		return animalsTypeList;
	}

	private void validateAnimalType(Class<?> classType)
			throws InvalidAnimalTypeException {
		if (!Animal.class.isAssignableFrom(classType)) {
			throw new InvalidAnimalTypeException();
		}
	}
}
