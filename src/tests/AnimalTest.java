package tests;

import static org.junit.Assert.*;
import model.Animal;
import model.Cat;
import model.Dog;

import org.junit.Test;

public class AnimalTest {

	@Test
	public void testDogSound() throws Exception {
		Animal dog = new Dog();

		assertEquals("auau", dog.getSound());
	}

	@Test
	public void testCatSound() throws Exception {
		Animal cat = new Cat();

		assertEquals("miau", cat.getSound());
	}
}
