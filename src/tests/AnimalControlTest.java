package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Animal;
import model.Cat;
import model.Dog;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import control.AnimalControl;
import exception.InvalidAnimalTypeException;

public class AnimalControlTest {

	private AnimalControl animalControl;

	@Before
	public void setUp() throws Exception {
		this.animalControl = new AnimalControl();
	}

	@After
	public void tearDown() throws Exception {
		this.animalControl = null;
	}

	@Test
	public void testNewAnimalControlIsEmpty() throws Exception {
		assertTrue(this.animalControl.isEmpty());
	}

	@Test
	public void testCountWhenAddAnimals() throws Exception {
		this.animalControl.add(new Cat());
		this.animalControl.add(new Dog());
		this.animalControl.add(new Dog());

		assertEquals(3, this.animalControl.count());
	}

	@Test
	public void testCountTypeWhenAddAnimals() throws Exception {
		this.animalControl.add(new Cat());
		this.animalControl.add(new Dog());
		this.animalControl.add(new Dog());

		assertEquals(1, this.animalControl.countType(Cat.class));
		assertEquals(2, this.animalControl.countType(Dog.class));
	}

	@Test
	public void testGetAnimalByIndex() throws Exception {
		Cat cat = new Cat();
		Dog dog = new Dog();
		this.animalControl.add(cat);
		this.animalControl.add(dog);
		this.animalControl.add(new Dog());

		assertEquals(cat, this.animalControl.get(0));
		assertEquals(dog, this.animalControl.get(1));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testThrowsExceptionWhenGetAnimalByInvalidIndex()
			throws Exception {
		this.animalControl.add(new Cat());
		this.animalControl.add(new Dog());
		this.animalControl.add(new Dog());

		this.animalControl.get(3);
	}

	@Test
	public void testGetListByAnimalType() throws Exception {
		Cat cat = new Cat();
		Dog dog = new Dog();

		ArrayList<Animal> catList = new ArrayList<Animal>();
		ArrayList<Animal> dogList = new ArrayList<Animal>();

		this.animalControl.add(cat);
		this.animalControl.add(dog);
		this.animalControl.add(dog);

		catList.add(cat);
		dogList.add(dog);
		dogList.add(dog);

		assertEquals(catList, this.animalControl.getAnimalsByType(Cat.class));
		assertEquals(dogList, this.animalControl.getAnimalsByType(Dog.class));
	}

	@Test(expected = InvalidAnimalTypeException.class)
	public void testThrowsExceptionWhenGetListByAnimalTypeWithInvalidAnimalType()
			throws Exception {
		this.animalControl.add(new Dog());

		this.animalControl.getAnimalsByType(Math.class);
	}
}
